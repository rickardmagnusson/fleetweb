﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fleet.Web.Dashboard
{
    public class DashboardAreaRegistration : AreaRegistration
    {

        public override string AreaName
        {
            get { return "Fleet.Web.Dashboard"; }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            
            context.MapRoute(
                 "Dashboard",
                 "",
                 new { controller="Home", action = "Index", id = UrlParameter.Optional },
                 namespaces: new string[] { "Fleet.Web.Dashboard.Controllers" }
             );


              context.MapRoute(
                 "Settings",
                 "{area}/{controller}/{action}/{id}",
                 new { area = "dashboard", controller = "Dashboard", action = "Index", id = UrlParameter.Optional },
                 namespaces: new string[] { "Fleet.Web.Dashboard.Controllers" }
                );
        }
    }
}