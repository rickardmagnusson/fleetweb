﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fleet.Web.Requisition
{
    public class RequisitionAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Fleet.Web.Requisition"; }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Requisition",
                "requisition/{controller}/{action}/{id}",
                new { controller="Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Fleet.Web.Requisition.Controllers" }
            );
        }
    }
}