﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fleet.Web.Maintenance.Controllers
{
    public class HomeController : Controller
    {
        [Item(0, "List make types")]
        public ActionResult ListMakeTypes()
        {
            return View();
        }

        [Item(0, "Create make type")]
        public ActionResult CreateMakeType()
        {
            return View();
        }

        [Item(1, "List manufacturers")]
        public ActionResult ListManufacturers()
        {
            return View();
        }

        [Item(1,"Create manufacturer")]
        public ActionResult CreateManufacturer()
        {
            return View();
        }

        [Item(2, "List suppliers")]
        public ActionResult ListSuppliers()
        {
            return View();
        }

        [Item(2,"Create suppliers")]
        public ActionResult CreateSupplier()
        {
            return View();
        }

        [Item(3,"List stations")]
        public ActionResult ListStations()
        {
            return View();
        }

        [Item(3,"Create station")]
        public ActionResult CreateStation()
        {
            return View();
        }

        [Item(4, "List colors")]
        public ActionResult ListColors()
        {
            return View();
        }

        [Item(4, "Create color")]
        public ActionResult CreateColor()
        {
            return View();
        }

        [Item(5, "List busines")]
        public ActionResult ListBusiness()
        {
            return View();
        }

        [Item(5, "Create business")]
        public ActionResult CreateBusiness()
        {
            return View();
        }
    }
}
