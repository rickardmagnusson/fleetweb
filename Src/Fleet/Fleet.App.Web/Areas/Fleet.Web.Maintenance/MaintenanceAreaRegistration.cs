﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fleet.Web.Maintenance
{
    public class MaintenanceAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Fleet.Web.Maintenance"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Maintenance",
                "maintenance/{controller}/{action}/{id}",
                new {controller = "home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Fleet.Web.Maintenance.Controllers" }
            );
        }
    }
}