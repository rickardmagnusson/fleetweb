﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fleet.Core.Persistance
{
    public static class PropertyReader
    {
        public static Dictionary<string, Int32> GetClassProperties(dynamic o)
        {
            var _dict = new Dictionary<string, Int32>();

            PropertyInfo[] props = Type.GetType(o.GetType().Name).GetProperties();
            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    ItemAttribute iAttr = attr as ItemAttribute;
                    if (iAttr != null)
                    {
                        Int32 pos = iAttr.Position;
                        string title = iAttr.Title;

                        _dict.Add(title, pos);
                    }
                }
            }
            return _dict;
        }
    }
}



