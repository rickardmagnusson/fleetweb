﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fleet.Core.Persistance
{
    public class ItemAttribute : Attribute
    {
        public Int32 Position { get; set; }
        public string Title { get; set;}

        public ItemAttribute(Int32 position, string title)
        {
            this.Position = position;
            this.Title = title;
        }
    }
}
