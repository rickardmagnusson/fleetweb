﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Fleet.Core.Persistance;
using Fleet.Core.Persistance.Xml;

namespace Fleet.App.Models.Models
{
    public class MenuListModel<TModel> where TModel : class
    {
        public MenuListModel()
        {
            var list = PropertyReader.GetClassProperties(typeof(TModel));
        }
    }

    public class Menu : Persistable<Menu.Data>
    {
        private Menu() : base() { }

        public string Title
        {
            get { return _data.Title; }
            set { _data.Title = value; }
        }

        public string Route
        {
            get { return _data.Route; }
            set { _data.Route = value; }
        }

        public string Position
        {
            get { return _data.Position; }
            set { _data.Position = value; }
        }

        public static Menu _instance;

        public static Menu GetInstance()
        {
            if (_instance == null) {
                _instance = new Menu();
                string dataFilePath = HttpContext.Current.Server.MapPath(_instance.GetDataFilename());

                if (File.Exists(dataFilePath)) {
                    _instance.LoadData();
                }
            }
            return _instance;
        }

        protected override string GetDataFilename()
        {
           return "~/App_Data/Menu.config";
        }

        public class Data
        {
            public string Title = string.Empty;
            public string Route = string.Empty;
            public string Position = string.Empty;
        }
    }
}