﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;


public static class SystemSettingsReader
{
    public static string Theme
    {
        get { return ApplyPath("Themes", "theme"); }
    }

    public static string ThemeScripts
    {
        get { return ApplyPathScript("Themes", "theme"); }
    }

    public static string BootstrapScripts
    {
        get { return ApplyPathScript("Themes", "Bootstrap"); }
    }

    public static string Bootstrap
    {
        get { return ApplyPath("Themes", "Bootstrap"); }
    }

    public static bool Debug
    {
        get {
            bool debug = false;
            Boolean.TryParse(AppKeyReader("Debug"), out debug);
            return debug;
        }
    }


    private static string ApplyPathScript(string path, string theme)
    {
        return string.Format("~/{0}/{1}/Scripts", path, AppKeyReader(theme));
    }

    private static string ApplyPath(string path, string theme)
    {
        return string.Format("~/{0}/{1}/Styles", path, AppKeyReader(theme));
    }

    public static string AppKeyReader(string key)
    {
        if (string.IsNullOrEmpty(key))
            return string.Empty;
        return ConfigurationManager.AppSettings[key];
    }
}
