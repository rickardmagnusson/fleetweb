﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fleet
{
    public class ItemAttribute : Attribute
    {
        Int32 _position;
        string _name;

        public ItemAttribute (Int32 position, string name){
            this._position = position;
            this._name = name;
        }
    }
}