﻿using System.Web;
using System.Web.Optimization;

namespace Fleet.App
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Fleet/Scripts")
                .IncludeDirectory(
                    SystemSettingsReader.ThemeScripts,
                    PathUtillity.JS
            ));

            bundles.Add(new ScriptBundle("~/Bootstrap/Scripts")
                .IncludeDirectory(
                    SystemSettingsReader.BootstrapScripts,
                    PathUtillity.JS
            ));

            bundles.Add(new StyleBundle("~/Bootstrap/Css")
              .IncludeDirectory(
                  SystemSettingsReader.Bootstrap,
                  PathUtillity.CSS
            ));

            bundles.Add(new StyleBundle("~/Fleet/Css")
                .IncludeDirectory(
                    SystemSettingsReader.Theme, 
                    PathUtillity.CSS
           ));
        }
    }
}